// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCMBb-CNjxd3Haak01ANT7vyHLthEoS3NY",
    authDomain: "hello-jce-isia.firebaseapp.com",
    databaseURL: "https://hello-jce-isia.firebaseio.com",
    projectId: "hello-jce-isia",
    storageBucket: "hello-jce-isia.appspot.com",
    messagingSenderId: "192700993128",
    appId: "1:192700993128:web:6d0e14ba96e3d2c9c3f5dd"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
